#include <SparkFun_ADXL345.h>         // SparkFun ADXL345 Library
#include <Servo.h>

void setup()
{
  Serial.begin(9600);
  indexFingerServo.attach(2);
  middleFingerServo.attach(3);
  thumbServo.attach(4);
  wristServoLeft.attach(6);
  wristServoRight.attach(7);
  forearmServo.attach(8);

  accelerometer.powerOn();
  accelerometer.setRangeSetting(4);
  accelerometer.setSpiBit(1);

  currentTimestamp = 0;
  previousTimestamp = 0;
  previousPhiAngle = 0;
  previousThetaAngle = 0;

  resetServos();

  Serial.println();
  Serial.println("START");
  Serial.println();
}

void loop() {
  currentTimestamp = millis();
  switch (appState)
  {
    case 0:
      if (currentTimestamp - previousTimestamp >= sensorsInitTime)
      {
        appState = 1;
        resetServos();
      }
      break;

    case 1:
      followMovements();
      break;

    default:
      break;
  }
}


class BendSensor
{
  private:
    int signalPin;
    int currentRead;
    int currentAngle;
    int minOfReadRange;
    int maxOfReadRange;
    int readRange;
    int resultSignal;

  public:
    BendSensor(int pin)
    {
      signalPin = pin;
      currentRead = 0;
      currentAngle = 0;
      minOfReadRange = 10000;
      maxOfReadRange = 0;
      readRange = 0;
      resultSignal = 0;
    }

    int getSignalForServo()
    {
      int tymczasoweWyliczenieSygnaluCzujnikaUgiecia = currentRead - minOfReadRange;
      resultSignal = map(tymczasoweWyliczenieSygnaluCzujnikaUgiecia, 0, readRange, 0, 180);
      if (resultSignal < 0) {
        resultSignal = 0;
      }

      if (resultSignal > 180) {
        resultSignal = 180;
      }

      return resultSignal;
    }

    void readSignal()
    {
      currentRead = analogRead(signalPin);
      updateReadRange();
    }

    void calculateAngle()
    {
      currentAngle = currentRead - minOfReadRange;
      currentAngle = map(currentAngle, 0, readRange, 0, 90);

      if (currentAngle < 0) {
        currentAngle = 0;
      }

      if (currentAngle > 90) {
        currentAngle = 90;
      }
    }

    void printAngle()
    {
      Serial.print("Angle at pin ");
      Serial.print(signalPin);
      Serial.print(" : ");
      Serial.print(currentAngle);
      Serial.print("\n\n");
    }

    void updateReadRange()
    {
      if (currentRead < minOfReadRange) {
        udpateMinOfReadRange(currentRead);
      }

      if (currentRead > maxOfReadRange) {
        udpateMaxOfReadRange(currentRead);
      }

      udpateReadRange();
    }

    void udpateReadRange()
    {
      readRange = maxOfReadRange - minOfReadRange;
    }

    void udpateMinOfReadRange(int read)
    {
      minOfReadRange = read;
    }

    void udpateMaxOfReadRange(int read)
    {
      maxOfReadRange = read;
    }
};

ADXL345 accelerometer = ADXL345();
Servo indexFingerServo;
Servo middleFingerServo;
Servo thumbServo;
Servo wristServoLeft;
Servo wristServoRight;
Servo forearmServo;
BendSensor indexFingerSensor(A1);
BendSensor middleFingerSensor(A2);
BendSensor thumbSensor(A3);

bool appState;
int xAxis, yAxis, zAxis;
int phiAngle, thetaAngle;
int previousPhiAngle, previousThetaAngle;
unsigned long currentTimestamp;
unsigned long previousTimestamp;
const unsigned int sensorsInitTime = 5000;

void readFromAccelerometer()
{
  accelerometer.readAccel(&xAxis, &yAxis, &zAxis);
  axisCorrection();
}

void axisCorrection()
{
  int tempValue = zAxis;
  zAxis = -xAxis;
  xAxis = tempValue;
}

void calculateAngles()
{
  phiAngle = atan2(yAxis, zAxis) * 57, 3;
  thetaAngle = atan2((-xAxis), sqrt(yAxis * yAxis + zAxis * zAxis)) * 57, 3;

  if (phiAngle < -90) {
    phiAngle = -90;
  }

  if (phiAngle > 90) {
    phiAngle = 90;
  }

  if (thetaAngle < -90) {
    thetaAngle = -90;
  }

  if (thetaAngle > 90) {
    thetaAngle = 90;
  }
}

void updatePreviousPhi()
{
  previousPhiAngle = phiAngle;
}

void updatePreviousTheta()
{
  previousThetaAngle = thetaAngle;
}

void readFromBendSensors()
{
  indexFingerSensor.readSignal();
  indexFingerSensor.calculateAngle();

  middleFingerSensor.readSignal();
  middleFingerSensor.calculateAngle();

  thumbSensor.readSignal();
  thumbSensor.calculateAngle();
}

void servosFollowBendSensors()
{
  indexFingerServo.write(180 - indexFingerSensor.getSignalForServo());

  middleFingerServo.write(180 - middleFingerSensor.getSignalForServo());

  thumbServo.write(180 - thumbSensor.getSignalForServo());
}

void servosFollowAccelerometer()
{
  int minimalMovementDiff = 8;

  if (abs(previousPhiAngle - phiAngle) >= minimalMovementDiff)
  {
    int wristSteeringValue = map(phiAngle, -90, 90, 0, 180);
    wristServoLeft.write(wristSteeringValue);
    wristServoRight.write(180 - wristSteeringValue);
    updatePreviousPhi();
  }
  
    if (abs(previousThetaAngle - thetaAngle) >= minimalMovementDiff)
  {
  int forearmSteeringValue = map(thetaAngle, -90, 90, 0, 180);
  forearmServo.write(forearmSteeringValue);
  updatePreviousTheta();
  }
}

void printSensorsInfo()
{
  Serial.println("Read from accelerometer [x,y,z, phiAngle, thetaAngle]: ");
  Serial.print(xAxis);
  Serial.print(", ");
  Serial.print(yAxis);
  Serial.print(", ");
  Serial.print(zAxis);
  Serial.print(", ");
  Serial.print(phiAngle);
  Serial.print(", ");
  Serial.println(thetaAngle);
  Serial.println();
  Serial.println();

  Serial.println("Read from fingers [index, middle, thumb]: ");
  Serial.print(indexFingerSensor.currentAngle);
  Serial.print(", ");
  Serial.print(middleFingerSensor.currentAngle);
  Serial.print(", ");
  Serial.println(thumbSensor.currentAngle);
  Serial.println();
  Serial.println();

}

void readSensorsInfo()
{
  readFromAccelerometer();
  calculateAngles();
  readFromBendSensors();
  printSensorsInfo();
}

void resetServos()
{
  wristServoLeft.write(90);
  wristServoRight.write(90);
  forearmServo.write(90);
  indexFingerServo.write(180);
  middleFingerServo.write(180);
  thumbServo.write(180);

}

void followMovements()
{
  readSensorsInfo();
  servosFollowAccelerometer();
  servosFollowBendSensors();
  delay(50);
}
